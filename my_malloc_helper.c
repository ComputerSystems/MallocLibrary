#include "my_malloc_helper.h"
//Initialize extern variables.
int PAGESIZE = 4096;
long stat_arr[MALLOC_ARENA_MAX][FREE_REQ_ID+1];
int num_arenas = 0;
//TLS
__thread int s_index = 0;

#ifdef GLOBAL_LOCK
pthread_mutex_t g_lock = PTHREAD_MUTEX_INITIALIZER;
#else
__thread pthread_mutex_t m_lock = PTHREAD_MUTEX_INITIALIZER;
#endif


void acquire_lock(){
	#ifdef GLOBAL_LOCK
	pthread_mutex_lock(&g_lock);	
	#else
	pthread_mutex_lock(&m_lock);	
	#endif
}

void release_lock(){
	#ifdef GLOBAL_LOCK
	pthread_mutex_unlock(&g_lock);	
	#else
	pthread_mutex_unlock(&m_lock);	
	#endif
}

void initialize_arena(){
	s_index = __atomic_fetch_add(&num_arenas, 1, __ATOMIC_SEQ_CST);
	if(s_index >= MALLOC_ARENA_MAX){
		n = snprintf(buff, BUFFSIZE, \
					"Error: Arenas Count(%d) Exceeds max Areana\n",\
					 s_index);
		write(STDOUT_FILENO, buff, n + 1);	
		exit(EXIT_FAILURE);		
	}
	__atomic_fetch_add(&(stat_arr[s_index][ALLOC_REQ_ID]), 1, __ATOMIC_SEQ_CST);
	START_ADDR = create_new_block();
}

void* create_new_block(){	
	PAGESIZE = sysconf(_SC_PAGESIZE);

	void *addr = mmap(0, PAGESIZE, PROT_READ | PROT_WRITE,
		           MAP_ANONYMOUS | MAP_PRIVATE, -1, 0);
	if(addr == MAP_FAILED){
		n = snprintf(buff, BUFFSIZE, "%s:%d mmap failed %d\n",
					__FILE__, __LINE__, errno);
		write(STDOUT_FILENO, buff, n + 1);
		errno = ENOMEM;
		return NULL;
	}
	__atomic_fetch_add(&(stat_arr[s_index][TOTAL_BYTES_ID]), \
						PAGESIZE, __ATOMIC_SEQ_CST);
	__atomic_fetch_add(&(stat_arr[s_index][TOTAL_BLOCKS_ID]), \
						1, __ATOMIC_SEQ_CST);
	#ifdef DBG
	n = snprintf(buff, BUFFSIZE, "Addr %p\n", addr);
	write(STDOUT_FILENO, buff, n + 1);	
	#endif
	MallocHeader *hdr = (MallocHeader*) addr;
	hdr->size = PAGESIZE;
	hdr->isFree = 1;
	hdr->isFirst = 1;
	#ifdef FAST_COALESCE
	hdr->prev = NULL;
	#endif
	hdr->next = NULL;
	return addr;
}

void* map_memory_region(size_t allocSize){
	
	acquire_lock();
	void *ret = mmap(0, allocSize, PROT_READ | PROT_WRITE,
		           MAP_ANONYMOUS | MAP_PRIVATE, -1, 0);
	if(ret == MAP_FAILED){
		n = snprintf(buff, BUFFSIZE, "%s:%d mmap failed %d\n",
					__FILE__, __LINE__, errno);
		write(STDOUT_FILENO, buff, n + 1);
		errno = ENOMEM;
		release_lock();
		return NULL;
	}
	#ifdef DBG
	n = snprintf(buff, BUFFSIZE, "%s:%d malloc: Allocated %zu bytes at %p\n",
		   __FILE__, __LINE__, allocSize, ret);
	write(STDOUT_FILENO, buff, n+1);
	#endif
	__atomic_fetch_add(&(stat_arr[s_index][ANON_BYTES_ID]), \
						allocSize, __ATOMIC_SEQ_CST);
	MallocHeader *hdr = (MallocHeader*) ret;
	hdr->size = allocSize;
	hdr->isFree = 0;
	#ifdef FAST_COALESCE
	hdr->prev = NULL;
	#endif	
	hdr->next = NULL;
	if((size_t)ret % 8 != 0){
		n = snprintf(buff, BUFFSIZE, "(%lx)Malloc At %p not aligned(%ld)\n",\
					 pthread_self(), ret, (size_t)ret%8);
		write(STDOUT_FILENO, buff, n + 1);		
	}
	release_lock();
	
	return ret + sizeof(MallocHeader);
}

size_t round_up(size_t allocSize){
	size_t size = MIN_BLOCK_SIZE;
	while(size < allocSize && size <= PAGESIZE){
		//Similar to pow(2)
		size <<= 1;
	}
	return size;
}

void* allocate(void *addr){
	MallocHeader* hdr = (MallocHeader*)addr;
	#ifdef DBG
	n = snprintf(buff, BUFFSIZE, "(%s:%d)Allocating %d At %p\n",
			__FILE__, __LINE__, hdr->size, addr);
	write(STDOUT_FILENO, buff, n + 1);		
	#endif
	hdr->isFree = 0;	
	return (addr+sizeof(MallocHeader));
}

void* split_and_allocate(void *addr, size_t size){
	MallocHeader *buddyA, *buddyB, *next_block;
	//Split the block by 2 (ex. if 128 => 64+64)
	buddyA = (MallocHeader*)addr;
	size_t nSize = (buddyA->size)/2;
	if(nSize < sizeof(MallocHeader)){
		n = snprintf(buff, BUFFSIZE, \
						"(%s:%d)Size went too low(%ld) FATAL.\n",\
						__FILE__, __LINE__, nSize);
		write(STDOUT_FILENO, buff, n + 1);	
		exit(EXIT_FAILURE);			
	}
	buddyB = (MallocHeader*)(addr+nSize);	
	#ifdef DBG
	n = snprintf(buff, BUFFSIZE, \
					"(%s:%d)creating buddies of size %ld at %p and %p\n",\
					__FILE__, __LINE__, nSize, addr, addr+nSize);
	write(STDOUT_FILENO, buff, n + 1);	
	#endif
	//update size, isFree, isFirst
	buddyA->size = buddyB->size = nSize;
	buddyA->isFree = 0;
	buddyB->isFree = 1;
	buddyA->isFirst = 1;
	buddyB->isFirst = 0;
	//update the next block addr
	next_block = buddyA->next;
	buddyA->next = addr+nSize;
	buddyB->next = next_block;
	#ifdef FAST_COALESCE
	//Update prev pointer
	buddyB->prev = addr;
	if(next_block != NULL){
		next_block->prev = addr+nSize;	
	}
	#endif
	__atomic_fetch_add(&(stat_arr[s_index][TOTAL_BLOCKS_ID]), \
						1, __ATOMIC_SEQ_CST);
	//We have found the required block
	if(nSize == size){
		return addr+sizeof(MallocHeader);
	}
	//split further
	else{
		return split_and_allocate(addr, size);	
	}	
}

void* allocate_new_block(size_t size, void *prev_blk_addr){
	void *addr = create_new_block();	
	if(addr == NULL){
		return NULL;	
	}
	MallocHeader *hdrOld = (MallocHeader*)prev_blk_addr;
	MallocHeader *hdrNew = (MallocHeader*) addr;		
	hdrOld->next = addr;
	#ifdef FAST_COALESCE
	hdrNew->prev = prev_blk_addr;
	#endif
	if(size == hdrNew->size){
		#ifdef DBG
		n = snprintf(buff, BUFFSIZE, "(%s:%d)Allocated %ld at %p\n",
				__FILE__, __LINE__, size, addr);
		write(STDOUT_FILENO, buff, n + 1);	
		#endif
		hdrNew->isFree = 0;
		return addr+sizeof(MallocHeader);
	}
	else if(size > hdrNew->size){
		//something went wrong.
		n = snprintf(buff, BUFFSIZE, "(%s:%d)Req size %ld Allocated size %ld\n",
				__FILE__, __LINE__, (size_t)size, (size_t)hdrNew->size);
		write(STDOUT_FILENO, buff, n + 1);	
		exit(EXIT_FAILURE);
	}
	else{
		return split_and_allocate(addr, size);	
	}
}

void update_ureqsize(void *addr, size_t size){
	if(!addr)
		return;
	MallocHeader *hdr = (MallocHeader*)(addr - sizeof(MallocHeader));
	hdr->uReqSize = size;
}

