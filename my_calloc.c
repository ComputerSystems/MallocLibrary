
#include "my_malloc_helper.h"

/*
The calloc() function allocates memory for an array of nmemb elements of 
size bytes each and returns a pointer to the allocated memory. 
The memory is set to zero. 

-> If nmemb or size is 0, 
	then calloc() returns either NULL, or a unique pointer value 
	that can later be successfully passed to free().
*/

static size_t multiply(size_t nmemb, size_t size){
	//multiplication must not overflow. Do safemultiplication.
	size_t res = nmemb * size;
	if(res/size != nmemb) 
		return 0;
	else
		return res;
}

void *calloc(size_t nmemb, size_t size){
	#ifdef DBG
	n = snprintf(buff, BUFFSIZE, \
					"Doing calloc for %zu memb and %zu size\n", \
					nmemb, size);
	write(STDOUT_FILENO, buff, n + 1);
	#endif
	//If nmemb/size is 0, then calloc() returns either NULL
	if(size <= 0 || nmemb <= 0)
		return NULL;
	size_t allocSize = multiply(nmemb, size);
	//overflow, return NULL set ENOMEM
	if(allocSize <= 0){
		errno = ENOMEM;
		return NULL;
	}
	void *addr = malloc(allocSize);
	if(!addr)
		return NULL;
	#ifdef DBG
	n = snprintf(buff, BUFFSIZE, "Doing calloc for  %zu at %p\n", \
					allocSize, addr);
	write(STDOUT_FILENO, buff, n + 1);	
	#endif

	acquire_lock();
	memset(addr, 0, allocSize);
	release_lock();
	
	#ifdef DBG_1
	n = snprintf(buff, BUFFSIZE, "(%lx)calloc done %zu at %p\n", \
					pthread_self(), allocSize, addr);
	write(STDOUT_FILENO, buff, n + 1);	
	#endif
	return addr;
}
