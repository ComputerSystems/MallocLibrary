#include "my_malloc_helper.h"
/*
The free() function frees the memory space pointed to by ptr, 
which must have been returned by a previous call to malloc()/calloc()/realloc()

-> Otherwise, or if free(ptr) has already been called before, 
	undefined behavior occurs. 
-> If ptr is NULL, 
	no operation is performed.
*/
static void coalesce(void *addr){

	if(!addr)
		return;

	MallocHeader *hdr = (MallocHeader*)addr;
	if(!hdr->next)
		return;	

	//First block of its buddy
	if(hdr->isFirst){
		MallocHeader *second = (MallocHeader*)(hdr->next);
		if(second->isFree && second->size == hdr->size &&\
			 abs(hdr->next-addr)== hdr->size){
			#ifdef DBG_1
			n = snprintf(buff, BUFFSIZE, \
							"(%s:%d) coalescing with secondblk (%d) (%p)\n",\
							 __FILE__, __LINE__, hdr->size, addr);
			write(STDOUT_FILENO, buff, n + 1);	
			#endif
			hdr->size = hdr->size * 2;
			hdr->next = second->next;
			#ifdef FAST_COALESCE
			if(second->next){
				MallocHeader *third = (MallocHeader*)(second->next);		
				third->prev = addr;
			}
			#endif
			__atomic_fetch_sub(&(stat_arr[s_index][TOTAL_BLOCKS_ID]), \
								1, __ATOMIC_SEQ_CST);
			return coalesce(hdr);
		}
	}
	//Second block of its buddy
	else{
		#ifdef FAST_COALESCE
		if(!hdr->prev){
			return;		
		}
		MallocHeader *first = (MallocHeader*)(hdr->prev);
		if(first->isFirst && first->isFree && first->size == hdr->size \
			&& abs(addr-hdr->prev) == hdr->size){
			#ifdef DBG_1
			n = snprintf(buff, BUFFSIZE, \
							"(%s:%d) coalescing with firstblk (%d) (%p)\n",\
							 __FILE__, __LINE__, hdr->size, addr);
			write(STDOUT_FILENO, buff, n + 1);	
			#endif
			first->size = first->size * 2;
			//Update Prev pointer of next(third) block
			MallocHeader *third = (MallocHeader*)(hdr->next);
			third->prev = hdr->prev;
			first->next = hdr->next;
			__atomic_fetch_sub(&(stat_arr[s_index][TOTAL_BLOCKS_ID]), \
								1, __ATOMIC_SEQ_CST);
			return coalesce(first);
		}
		#else
		return;	
		#endif
	}
	return;
}

void free(void *addr){

	#ifdef DBG
	n = snprintf(buff, BUFFSIZE, "Freeing at %p\n", addr);
	write(STDOUT_FILENO, buff, n + 1);	
	#endif
	
	//If addr is NULL, no Operation is performed.
	if(addr == NULL)
		return;
	
	acquire_lock();
	__atomic_fetch_add(&(stat_arr[s_index][FREE_REQ_ID]), 1, __ATOMIC_SEQ_CST);
	void *hdr_addr = addr - sizeof(MallocHeader);
	MallocHeader *hdr = (MallocHeader*)hdr_addr;

	//Double free. Crash.
	assert(hdr->isFree != 1);	

	hdr->isFree = 1;
	size_t hdr_size = -1*hdr->size;
	if(hdr->size >= PAGESIZE){
		__atomic_fetch_sub(&(stat_arr[s_index][ANON_BYTES_ID]), \
							hdr->size, __ATOMIC_SEQ_CST);
		munmap(addr, hdr->size);	
	}
	else{
		__atomic_fetch_sub(&(stat_arr[s_index][USED_BLOCKS_ID]), \
							1, __ATOMIC_SEQ_CST);
		#ifdef USER_ALLOC_STATS
		__atomic_fetch_sub(&(stat_arr[s_index][USED_BYTES_ID]), \
							hdr->uReqSize, __ATOMIC_SEQ_CST);
		#else
		__atomic_fetch_sub(&(stat_arr[s_index][USED_BYTES_ID]), \
							hdr->size, __ATOMIC_SEQ_CST);
		#endif	
		coalesce(hdr_addr);
	}
	release_lock();
	#ifdef DBG_1
	n = snprintf(buff, BUFFSIZE, "(%lx)Freed from %p\n", \
					pthread_self(), hdr_addr);
	write(STDOUT_FILENO, buff, n + 1);	
	#endif
	return;
}
