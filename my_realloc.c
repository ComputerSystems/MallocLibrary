
#include "my_malloc_helper.h"

/*
The realloc() function changes the size of the memory block pointed to by ptr 
to size bytes. The contents will be unchanged in the range from the start 
of the region up to the minimum of the old and new sizes. 

-> If the new size is larger than the old size, 
	the added memory will not be initialized. 
-> If ptr is NULL, 
	then the call is equivalent to malloc(size), for all values of size; 
-> if size is equal to zero, and ptr is not NULL, 
	then the call is equivalent to free(ptr).
-> Unless ptr is NULL, 
	it must have been returned by an earlier call to malloc()/calloc()/realloc()
-> If the area pointed to was moved, a free(ptr) is done.

*/

static void* unaligned_memcpy(void *dst, const void* src, size_t size){
	char *dest = (char *)dst;
	char *source = (char *)src;
	size_t i = 0;
	for(; i < size; i++){
		dest[i] = source[i];	
	}
	return dst;
}

void *realloc(void *addr, size_t size){
	#ifdef DBG
	n = snprintf(buff, BUFFSIZE, "Doing realloc for  %zu at %p\n", \
			size, addr);
	write(STDOUT_FILENO, buff, n + 1);	
	#endif
	if(addr == NULL)
		return malloc(size);
	
	if(size <= 0)
		return NULL;
	
	//Get the header
	void *hdr_addr = addr - sizeof(MallocHeader);
	MallocHeader *hdr = (MallocHeader*)hdr_addr;
	//Already bigger than asking size
	if(hdr->size > size){
		hdr->uReqSize = size;
		return hdr_addr;		
	}
	
	void *new_addr = malloc(size);
	if(!new_addr)
		return NULL;
	#ifdef DBG
	n = snprintf(buff, BUFFSIZE, "Doing memcpy from %p\n", addr);
	write(STDOUT_FILENO, buff, n + 1);	
	#endif
	
	acquire_lock();
	if(hdr->size > sizeof(MallocHeader)){
		unaligned_memcpy(new_addr, addr, hdr->size - sizeof(MallocHeader));
	}	
	else{
		#ifdef DBG
		n = snprintf(buff, BUFFSIZE, "HDR size is 0 (%p)\n", hdr_addr);
		write(STDOUT_FILENO, buff, n + 1);	
		#endif
	}
	release_lock();
	
	free(addr);
	#ifdef DBG_1
	n = snprintf(buff, BUFFSIZE, "(%lx)Done realloc for  %zu at %p\n", \
			pthread_self(), size, addr);
	write(STDOUT_FILENO, buff, n + 1);	
	#endif
	return new_addr;
}
