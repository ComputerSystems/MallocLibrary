#include "my_malloc.h"

//Helper function for malloc library

void initialize_arena();
void* create_new_block();
void* map_memory_region(size_t allocSize);
size_t round_up(size_t allocSize);
void* allocate(void *addr);
void* split_and_allocate(void *addr, size_t size);
void* allocate_new_block(size_t size, void *prev_blk_addr);
void update_ureqsize(void *addr, size_t size);
void acquire_lock();
void release_lock();
