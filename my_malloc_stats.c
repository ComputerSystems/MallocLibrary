#include "my_malloc_helper.h"

#ifdef DBG
void print_blocks(){
	void *tmp_SA = START_ADDR;
	n = snprintf(buff, BUFFSIZE, "START PRINTING BLOCKS\n");
	write(STDOUT_FILENO, buff, n + 1);	
	while(tmp_SA != NULL){
		MallocHeader *hdr = (MallocHeader*)tmp_SA;
		#ifdef FAST_COALESCE
		n = snprintf(buff, BUFFSIZE, "Size %d isFree %d isFirst %d prev %p\n",
					hdr->size, hdr->isFree, hdr->isFirst, hdr->prev);
		#else
		n = snprintf(buff, BUFFSIZE, "Size %d isFree %d isFirst %d\n",
					hdr->size, hdr->isFree, hdr->isFirst);
		#endif	
		write(STDOUT_FILENO, buff, n + 1);		
		tmp_SA = hdr->next;
	}
	n = snprintf(buff, BUFFSIZE, "END PRINTING BLOCKS\n");
	write(STDOUT_FILENO, buff, n + 1);		
}
#endif

void malloc_stats(){
	if(num_arenas == 0)
		return;
	acquire_lock();
	int i;
	size_t total_size = 0;
	for(i = 0; i < num_arenas; i++){
		total_size += stat_arr[i][TOTAL_BYTES_ID];
		n = snprintf(buff, BUFFSIZE, "***Arena: %d***\nTotal Bytes: %ld\
					\nUsed Bytes: %ld\nFree Bytes: %ld\nAnon Bytes: %ld\
					\nUsed Blocks: %ld\nFree Blocks: %ld\
					\nAlloc Requests: %ld\nFree Requests: %ld\n\n",
					i,
					stat_arr[i][TOTAL_BYTES_ID], 
					stat_arr[i][USED_BYTES_ID], 
					stat_arr[i][TOTAL_BYTES_ID] - stat_arr[i][USED_BYTES_ID], 
					stat_arr[i][ANON_BYTES_ID], 
					stat_arr[i][USED_BLOCKS_ID], 
					stat_arr[i][TOTAL_BLOCKS_ID] - stat_arr[i][USED_BLOCKS_ID],
					stat_arr[i][ALLOC_REQ_ID], 
					stat_arr[i][FREE_REQ_ID]);
		write(STDOUT_FILENO, buff, n + 1);	
	}
	n = snprintf(buff, BUFFSIZE, "Total Arenas: %d\nTotal Size: %ld\n",
					num_arenas, total_size);
	write(STDOUT_FILENO, buff, n + 1);		
	release_lock();
}

struct mallinfo mallinfo(){
	struct mallinfo mi = {0,0,0,0,0};
	if(num_arenas == 0)
		return mi;
	acquire_lock();
	int i;
	for(i = 0; i < num_arenas; i++){
		mi.arena += stat_arr[i][TOTAL_BYTES_ID];
		mi.hblkhd += stat_arr[i][ANON_BYTES_ID];
		mi.ordblks += (stat_arr[i][TOTAL_BLOCKS_ID] - \
							stat_arr[i][USED_BLOCKS_ID]);
		mi.uordblks += stat_arr[i][USED_BYTES_ID];
	}
	mi.fordblks = mi.arena - mi.uordblks;
	release_lock();
	return mi;
}
