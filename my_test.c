#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/mman.h>
#include <unistd.h>
#include <math.h>

#include "my_malloc.h"

int main(int argc, char **argv){
	char *arr1 = my_malloc(10);
	for(int i = 0; i < 10; i++){
		arr1[i] = 'a';	
	}
	arr1[9] = '\0';
	n = snprintf(buff, BUFFSIZE, "%s is the string\n", arr1);
	write(STDOUT_FILENO, buff, n+1);
	print_blocks();

	my_free(arr1);
	print_blocks();
	arr1 = my_malloc(10);
	print_blocks();
	for(int i = 0; i < 10; i++){
		arr1[i] = 'b';	
	}
	arr1[9] = '\0';
	n = snprintf(buff, BUFFSIZE, "%s is the string\n", arr1);
	write(STDOUT_FILENO, buff, n+1);

	int *arr2 = my_calloc(5, sizeof(int));
	n = snprintf(buff, BUFFSIZE, "%d %d %d %d %d\n", arr2[0], arr2[1], arr2[2], arr2[3], arr2[4]);
	write(STDOUT_FILENO, buff, n+1);
	print_blocks();
	for(int i = 0; i < 5; i++){
		arr2[i] = 2;	
	}
	n = snprintf(buff, BUFFSIZE, "%d %d %d %d %d\n", arr2[0], arr2[1], arr2[2], arr2[3], arr2[4]);
	write(STDOUT_FILENO, buff, n+1);

	arr2 = my_realloc(arr2, 80);
	n = snprintf(buff, BUFFSIZE, "%d %d %d %d %d %d %d %d %d %d\n", arr2[0], arr2[1], arr2[2], arr2[3], arr2[4], arr2[5], arr2[6], arr2[7], arr2[8], arr2[9]);
	write(STDOUT_FILENO, buff, n+1);
	print_blocks();
	for(int i = 0; i < 10; i++){
		arr2[i] = 3;	
	}
	n = snprintf(buff, BUFFSIZE, "%d %d %d %d %d %d %d %d %d %d\n", arr2[0], arr2[1], arr2[2], arr2[3], arr2[4], arr2[5], arr2[6], arr2[7], arr2[8], arr2[9]);
	write(STDOUT_FILENO, buff, n+1);
	int *arr3 = my_malloc(5000);
	n = snprintf(buff, BUFFSIZE, "%p is the mapped address\n", arr3);
	write(STDOUT_FILENO, buff, n+1);
	my_free(arr3);

	int *arr4 = my_malloc(2500);
	print_blocks();
	my_free(arr4);
	print_blocks();
	return 0;
}
