#include "my_malloc_helper.h"

__thread void *START_ADDR = NULL;

/*
The malloc() function allocates size bytes and returns a pointer 
to the allocated memory. The memory is not initialized. 
-> If size is 0, 
	then malloc() returns either NULL, or a unique pointer value 
	that can later be successfully passed to free().
*/
static void update_stats_arr(void *ret, size_t size, size_t reqSize){
	__atomic_fetch_add(&(stat_arr[s_index][USED_BLOCKS_ID]), \
							1, __ATOMIC_SEQ_CST);
	#ifdef USER_ALLOC_STATS
	update_ureqsize(ret, size);		
	__atomic_fetch_add(&(stat_arr[s_index][USED_BYTES_ID]), \
						size, __ATOMIC_SEQ_CST);
	#else
	__atomic_fetch_add(&(stat_arr[s_index][USED_BYTES_ID]), \
						reqSize, __ATOMIC_SEQ_CST);
	#endif	
}

void *malloc(size_t size)
{
	//If size is 0, then malloc() returns either NULL, or a unique pointer value
	if(size <= 0)
		return NULL;

	if(!START_ADDR){
		initialize_arena();
		if(START_ADDR == NULL)
			return NULL;	
	}
	else
		__atomic_fetch_add(&(stat_arr[s_index][ALLOC_REQ_ID]), \
							1, __ATOMIC_SEQ_CST);
		
	size_t allocSize = size + sizeof(MallocHeader);
	size_t reqSize = round_up(allocSize);
	//If requested size is greater than page size, allocate a new memory region	
	if(allocSize >= PAGESIZE){
		return map_memory_region(allocSize);			
	}	
	if(reqSize >= PAGESIZE){
		return map_memory_region(reqSize);			
	}
	
	acquire_lock();
	void *tmp_SA = START_ADDR;
	void *prev_blk_addr = tmp_SA;
	void *ret = NULL;
	while(tmp_SA != NULL){
		MallocHeader *hdr = (MallocHeader*) tmp_SA;
		if(hdr->isFree){
			//Found appropriate block
			if(hdr->size == reqSize){
				ret = allocate(tmp_SA);
				break;
			}
			//Found a bigger block. so split it
			if(hdr->size > reqSize){
				ret = split_and_allocate(tmp_SA, reqSize);
				break;
			}			
		}
		prev_blk_addr = tmp_SA;
		tmp_SA = hdr->next;		
	}
	//We have run out of freeblocks. Allocate a new block
	if(tmp_SA == NULL){
		ret = allocate_new_block(reqSize, prev_blk_addr);	
	}	
	update_stats_arr(ret, size, reqSize);
	release_lock();
	return ret;
}

