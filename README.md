# Malloc Library

### Introduction
Implementing our own Malloc Library based on Buddy Allocation system and Per-Thread Malloc Arena.

### Requirements
1. Buddy Allocation System
2. Per-Thread Malloc Arena for thread safety
3. Functions to be supported => malloc, free, calloc, realloc, malloc_stats, mallinfo similar to the standard glibc malloc library.
4. Address to be returned  must be 8-byte aligned. (Ex. 4000, 4008 Not something like 5111)
5. Fork() support
6. Use sbrk/mmap to request memory from kernel. All memory requests should be in PAGESIZE

### Design Decisions

Following are some design decision I had to make:

##### Free List
Basic DS for the free list is going to be a Doubly Linked List.
1. Each node has variables to hold size of the bin
2. It also has variables to denote if the bin is free and if it is the first bin of its buddy.
3. It has pointers to previous and next node.
Allocation and modification is done just by flipping/splitting/merging the variables of the nodes in the list.

##### Thread-Local Storage
Each thread will have its own free list so that they don't contend with other threads looking for allocating/freeing memory.
We use Thread-Local storage class(__thread) to achieve this. Each thread is allocated some memory that we call as Arena. 
And the threads are allowed only to modify the memory in its own arena.
There is an index variable that tells the ID of the arena.
We also store a thread local lock for each arena.

##### No sbrk only mmap
We are not at all incrementing program break to provide memory. We only use mmap(Mainly for simplicity).

sbrk allocates contiguous memory in heap, It is possible for some threads to exceed the initially allocated memory using sbrk.
In that case, we need to increment program break and link it with the particular thread. 
And allocation should be careful so that it doesn't messup someother thread's arena. To avoid such manipulation in the heap, we use mmap.

In case of mmap, We use multiple chunks of mmaped memory and append it to the free list when a thread needs more memory. 
When user asks memory >= PAGESIZE, we allocate a whole memory region and return.

##### Statistics array for each arena 
We maintain a 2D array.
This array hold stats for the fixed number of arenas. 
We also maintain a struct called mallinfo that gives cumulative stats of all arenas to the user.

##### Fixed Malloc Arena
Number of Arena we can create is fixed. It is controlled by MALLOC_ARENA_MAX variable. 
Because, we have to provide stats for each arena and we maintain it in a 2D array.
Exceeding this count by creating more threads will kill the program with failure.

##### Byte alignment
Size of the struct is multiple of 8Bytes regardless of 32bit or 64bit machine. Also the bin size is > 8 and a multiple of 8. 
So we will always return memory address that is 8B aligned.

### Implementation

##### Data Structures

###### MallocHeader - Meta data of each allocated blocks 
```
typedef struct MallocHeader
{	
	//size of the allocated block/bin  
	//In Arena: 32B-PAGESIZE/2B; Anon Mmap - upto 2^60(if supported by system)
	size_t size:62;
	//If the block is free or allocated
	size_t isFree:1;
	//Is the first/second part of buddy
	size_t isFirst:1;
	//Address of next block(We are using mmap so address is not contiguous)
	void *next;
	#ifdef FAST_COALESCE
	//Address of previous block(Disable if smaller header size is preferred)
	void *prev;
	#endif
	#ifdef USER_ALLOC_STATS
	//Actual Size requested by user.
	size_t uReqSize;
	#endif
} MallocHeader;
```
Normally, we measure the memory stats in the size of bin we allocate.
Here, If we want to measure the actual size of memory requests of user, we can enable this USER_ALLOC_STATS macro.

###### MallInfo structure
```
struct mallinfo{
	//size of arena (Except the mmaped regions)
	long arena;
	//size of mmaped regions(I.e. Allocation >= PAGESIZE)
	long hblkhd;
	//# of free chunks
	long ordblks;
	//used size (within Arena)
	long uordblks;
	//free size (within Arena)
	long fordblks;	
};
```
Here, hblkhd is the total size of mmap requests that are >= Pagesize.
All sizes are in Bytes.

##### 1. Initializing Arena
When we get the first malloc request for the arena,
We request memory using mmap and initialize the free list with this newly got memory block as its first element.
Here, we set arena ID as num_arenas and increment num_arenas.

##### 2. Allocation
1. If the request is >= PAGESIZE, we allocate a whole mmap region.
2. If not, we add the MallocHeader size to the actual request.
3. Then, We roundup the size to power of 2. (Ex. 32, 64, 128 etc) and start looking for the block in our free list.
4. If we have a free block of requested size in our list, we allocate it and mark it as "Not Free".
5. If we have a bigger chunk, we split the free block and see if it can be allocated. (During split, we mark the first block as first of its buddy)
6. We repeat step 5 untill we find appropriate block.
Upon success, we return the address to the user. Upon failure to allocate memory, we set errno to ENOMEM and return NULL.

We have to add the MallocHeader before every memory block we allocate and return to user.
This header will hold the size of the bin we have allocated. Also, we can store the actual memory request from user.

##### 3. Free
1. When we get the address, we see if the memory address is not NULL. 
2. Then, we go extract the header of its particular block (Do addr - sizeof MallocHeader)
3. If the size is >= PAGESIZE, do munmap
4. Else mark it free, coalesce if possible and return

##### 4. Coalesce
1. When we get free request, we check if its first of its buddy.
2. We merge it with the next block if that block is free and of same size as the one freed.
3. If it is not the first of its buddy, we check the previous block and do the same.
4. We repeat this untill there is no coalesce is possible.

##### 5. Calloc
1. In calloc, we do safe multiplication to avoid multiplication overflow.
2. After this this is same as malloc + memset

##### 6. Realloc
1. In realloc, we check if the requested size can be fulfilled in the available block. 
2. If not, we allocate new memory block and memcpy the old data to the new region and free the old region.

##### 7. malloc_stats
Based on the alloc/free request, we udpate the corresponding value in the stats_array.
When malloc_stats is called, we go through the array and print the statistics.
In case of mallinfo, we do the same but instead of printing, we populate mallinfo struct and give it to user.

### Known Bugs
This program was tested against the benchmark file(http://locklessinc.com/downloads/t-test1.c) with TOTAL set to 50. 
Also, tested the fork support and those work fine. 

1. We can't create more than MALLOC_ARENA_MAX arenas. 
2. Also, we dont clean up the arenas once the thread has died. So we are likely to endup with out of memory sooner.
3. MallocStats is thread safe but not synchronized. You are not guaranteed to
   see the fresh data as someother threads may be updating the value after we
read that particular arena's value.

One way to solve this is by enabling global lock macro. But again this violates
per-thread arena concept
4. This program is slower than standard Glibc Malloc Library.
