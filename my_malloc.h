#ifndef MY_MALLOC_H_
#define MY_MALLOC_H_

#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/mman.h>
#include <unistd.h>
#include <errno.h>
#include <math.h>
#include <pthread.h>

//Disable if you want the header size to be smaller
#define FAST_COALESCE 1

//Enable if a Global lock is needed
//#define GLOBAL_LOCK 1

//Enable this if you want to keep track of allocation made by User program
//Disable if you want to keep track of allocations made by malloc library
#define USER_ALLOC_STATS 1

//For debugging purposes
//#define DBG
//#define DBG_1

#define BUFFSIZE 1024
#define MIN_BLOCK_SIZE 32
#define MALLOC_ARENA_MAX 10000

#define TOTAL_BYTES_ID 0
#define ANON_BYTES_ID 1
#define USED_BYTES_ID 2
#define TOTAL_BLOCKS_ID 3
#define USED_BLOCKS_ID  4
#define ALLOC_REQ_ID 5
#define FREE_REQ_ID 6

//stat array
extern long stat_arr[MALLOC_ARENA_MAX][FREE_REQ_ID+1];
//Total Number of arenas created so far
extern int num_arenas;
//Pagesize is computed for each machine 
extern int PAGESIZE;

//TLS for stat array index
extern __thread int s_index;
extern __thread void *START_ADDR;

#ifdef GLOBAL_LOCK
//Global Lock (In case of single arena)
extern pthread_mutex_t g_lock;
#else
//Thread Local lock
extern __thread pthread_mutex_t m_lock;
#endif

//For debugging.
//#define DBG
//#define DBG_1
char buff[BUFFSIZE];
int n;

//MallocHeader - Meta data of each allocated blocks (16B)
typedef struct MallocHeader
{	
	//size of the allocated block/bin  
	//In Arena: 32B-PAGESIZE/2B; Anon Mmap - upto 2^60(if supported by system)
	size_t size:62;
	//If the block is free or allocated
	size_t isFree:1;
	//Is the first/second part of buddy
	size_t isFirst:1;
	//Address of next block(We are using mmap so address is not contiguous)
	void *next;
	#ifdef FAST_COALESCE
	//Address of previous block(Disable if smaller header size is preferred)
	void *prev;
	#endif
	#ifdef USER_ALLOC_STATS
	//Actual Size requested by user.
	size_t uReqSize;
	#endif
} MallocHeader;

//MallInfo structure
struct mallinfo{
	//size of arena (Except the mmaped regions)
	long arena;
	//size of mmaped regions(I.e. Allocation > PAGESIZE)
	long hblkhd;
	//# free chunks
	long ordblks;
	//used size (in Arena)
	long uordblks;
	//free size (in Arena)
	long fordblks;	
};

void *malloc(size_t size);
void free(void *addr);
void *calloc(size_t nmemb, size_t size);
void *realloc(void *addr, size_t size);
void malloc_stats();
struct mallinfo mallinfo();
#ifdef DBG
void print_blocks();
#endif

#endif
