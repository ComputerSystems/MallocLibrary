#Sample Makefile for Malloc
CC=gcc
CFLAGS=-g -O0 -fPIC -lm -pthread

all:	check

clean:
	rm -rf libmalloc.so *.o my_malloc core test ptest test1

libmalloc.so: my_malloc.o my_free.o my_calloc.o my_realloc.o my_malloc_helper.o\
				my_malloc_stats.o
	$(CC) $(CFLAGS) -shared -Wl,--unresolved-symbols=ignore-all $^ -o $@

test: test.o
	$(CC) $(CFLAGS) $< -o $@

ptest: ptest.o
	$(CC) $(CFLAGS) $< -o $@

# For every XYZ.c file, generate XYZ.o.
%.o: %.c
	$(CC) $(CFLAGS) $< -c -o $@

check:	libmalloc.so test
	LD_PRELOAD=`pwd`/libmalloc.so ./test

pcheck:	libmalloc.so ptest
	LD_PRELOAD=`pwd`/libmalloc.so ./ptest

check1:	libmalloc.so test1
	LD_PRELOAD=`pwd`/libmalloc.so ./test1

dist: clean
	dir=`basename $$PWD`; cd ..; tar cvf $$dir.tar ./$$dir; gzip $$dir.tar
